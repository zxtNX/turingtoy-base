from typing import (
    Dict,
    List,
    Literal,
    Optional,
    Tuple,
    TypedDict,
    Union,
)

import poetry_version

__version__ = poetry_version.extract(source_file=__file__)


# Définition des types littéraux pour les directions et les opérations de la machine de Turing
Direction = Literal["L", "R"]
Operation = Literal["write", Direction]
Instruction = Union[Direction, Dict[Operation, str]]

# Définition de la table de transition comme un dictionnaire imbriqué
TransitionTable = Dict[str, Dict[str, Instruction]]

# Définition de la configuration de la machine de Turing à l'aide de TypedDict
MachineConfig = TypedDict(
    "MachineConfig",
    {
        "blank": str,                  # Symbole représentant le blanc
        "start state": str,            # État de départ de la machine
        "final states": List[str],     # Liste des états finaux
        "table": TransitionTable,      # Table de transition
    },
)


# Définition d'une entrée d'historique pour suivre l'exécution de la machine de Turing
class HistoryEntry(TypedDict):
    state: str        # État courant
    reading: str      # Symbole lu
    position: int     # Position courante sur la bande
    memory: str       # État de la bande mémoire
    transition: Instruction  # Transition effectuée


def run_turing_machine(
    machine: MachineConfig,
    input_str: str,
    steps: Optional[int] = None,
) -> Tuple[str, List[HistoryEntry], bool]:
    """
    Exécute une machine de Turing sur une chaîne d'entrée donnée.

    Args:
        machine (MachineConfig): Configuration de la machine de Turing.
        input_str (str): Chaîne d'entrée à traiter par la machine de Turing.
        steps (Optional[int]): Nombre maximal d'étapes à exécuter (facultatif).

    Returns:
        Tuple[str, List[HistoryEntry], bool]: 
            - La mémoire finale de la bande après l'exécution.
            - L'historique des étapes exécutées.
            - Un booléen indiquant si la machine a atteint un état final.
    """

    # Initialisation des variables à partir de la configuration de la machine
    transition_table: TransitionTable = machine["table"]
    final_states: List[str] = machine["final states"]
    blank_symbol: str = machine["blank"]

    # Conversion de la chaîne d'entrée en liste pour manipulation
    memory: List[str] = list(input_str)
    history: List[HistoryEntry] = []
    state: str = machine["start state"]
    position: int = 0
    steps_taken: int = 0

    # Boucle principale d'exécution de la machine de Turing
    while state not in final_states:
        # Gestion des débordements à gauche et à droite de la bande mémoire
        if position < 0:
            memory.insert(0, blank_symbol)
            position = 0
        elif position >= len(memory):
            memory.append(blank_symbol)

        # Lecture du symbole courant
        symbol: str = memory[position]

        # Récupération de l'instruction de transition correspondante
        instruction: Instruction = transition_table[state][symbol]

        # Enregistrement de l'état actuel dans l'historique
        history.append(
            HistoryEntry(
                state=state,
                reading=symbol,
                position=position,
                memory="".join(memory),
                transition=instruction,
            )
        )

        # Exécution de l'instruction de transition
        if isinstance(instruction, str):
            if instruction == "L":
                position -= 1
            else:
                position += 1
        else:
            memory[position] = instruction.get("write", symbol)
            if "L" in instruction:
                position -= 1
                state = instruction["L"]
            else:
                position += 1
                state = instruction["R"]

        steps_taken += 1

    # Retour de l'état final de la bande mémoire, de l'historique et de l'indicateur d'état final atteint
    return "".join(memory).strip(blank_symbol), history, state in final_states
